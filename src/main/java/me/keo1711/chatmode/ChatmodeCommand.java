package me.keo1711.chatmode;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class ChatmodeCommand extends Command {

	public ChatmodeCommand() {
		super("chatmode");
	}

	@Override
	public void execute(CommandSender commandSender, String[] args) {
		commandSender.sendMessage(new ComponentBuilder("Usage: /chatmode <player>").color(ChatColor.RED).create());
		ProxiedPlayer p = ProxyServer.getInstance().getPlayer(args[0]);
		if (args.length == 0) {
			commandSender.sendMessage(new ComponentBuilder("Error: User not online").color(ChatColor.RED).create());
			return;
		}
	}
}
