package me.keo1711.chatmode;

import net.md_5.bungee.api.plugin.Plugin;

public class ChatMode extends Plugin {

	@Override
	public void onEnable() {
		getLogger().info("[ChatMode] is now enabled");
		getProxy().getPluginManager().registerCommand(this, new ChatmodeCommand());
	}

	@Override
	public void onDisable() {
		getLogger().info("[ChatMode] is now disabled");
	}

}